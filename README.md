## fairchat App

React-native based mobile chat app for Android and iOS.
Connects to any Rocket.Chat server v0.70+ via meteor DDP and REST API.

Customized version for fairchat.net provided by osAlliance.com
Forked from https://github.com/RocketChat/Rocket.Chat.ReactNative

## License

MIT

## Issues & Feature requests

https://git.fairkom.net/chat/fairchat.ReactNative

## Contact

fairkom is an official Rocket.Chat partner and offers customized versions or integrations.
Talk to us, if you would like to see more features or need your own app: sales@fairkom.eu

## Features

| Feature                                                       | Status |
| ------------------------------------------------------------- | ------ |
| Jitsi Video Integration                                       | ✅     |
| Federation (Directory)                                        | ✅     |
| Discussions                                                   | ❌     |
| Omnichannel                                                   | ❌     |
| Threads                                                       | ✅     |
| Record Audio                                                  | ✅     |
| Record Video                                                  | ✅     |
| Commands                                                      | ✅     |
| Draft message per room                                        | ✅     |
| Share Extension                                               | ✅     |
| Notifications Preferences                                     | ✅     |
| Edited status                                                 | ✅     |
| Upload video                                                  | ✅     |
| Grouped messages                                              | ✅     |
| Mark room as read                                             | ✅     |
| Mark room as unread                                           | ✅     |
| Tablet Support                                                | ✅     |
| Read receipt                                                  | ✅     |
| Broadbast Channel                                             | ✅     |
| Authentication via SAML                                       | ✅     |
| Authentication via CAS                                        | ✅     |
| Custom Fields on Signup                                       | ✅     |
| Report message                                                | ✅     |
| Theming                                                       | ✅     |
| Settings -> Review the App                                    | ✅     |
| Settings -> Default Browser                                   | ❌     |
| Admin panel                                                   | ✅     |
| Reply message from notification                               | ✅     |
| Unread counter banner on message list                         | ✅     |
| E2E Encryption                                                | ❌     |
| Join a Protected Room                                         | ❌     |
| Optional Analytics                                            | ❌     |
| Settings -> About us                                          | ❌     |
| Settings -> Contact us                                        | ✅     |
| Settings -> Update App Icon                                   | ❌     |
| Settings -> Share                                             | ❌     |
| Accessibility (Medium)                                        | ❌     |
| Accessibility (Advanced)                                      | ❌     |
| Authentication via Meteor                                     | ❌     |
| Authentication via Wordpress                                  | ✅     |
| Authentication via Custom OAuth                               | ✅     |
| Add user to the room                                          | ✅     |
| Send message                                                  | ✅     |
| Authentication via Email                                      | ✅     |
| Authentication via Username                                   | ✅     |
| Authentication via LDAP                                       | ✅     |
| Message format: Markdown                                      | ✅     |
| Message format: System messages (Welcome, Message removed...) | ✅     |
| Message format: links                                         | ✅     |
| Message format: images                                        | ✅     |
| Message format: replies                                       | ✅     |
| Message format: alias with custom message (title & text)      | ✅     |
| Messages list: day separation                                 | ✅     |
| Messages list: load more on scroll                            | ✅     |
| Messages list: receive new messages via subscription          | ✅     |
| Subscriptions list                                            | ✅     |
| Segmented subscriptions list: Favorites                       | ✅     |
| Segmented subscriptions list: Unreads                         | ✅     |
| Segmented subscriptions list: DMs                             | ✅     |
| Segmented subscriptions list: Channels                        | ✅     |
| Subscriptions list: update user status via subscription       | ✅     |
| Numbers os messages unread in the Subscriptions list          | ✅     |
| Status change                                                 | ✅     |
| Upload image                                                  | ✅     |
| Take picture & upload it                                      | ✅     |
| 2FA                                                           | ✅     |
| Signup                                                        | ✅     |
| Autocomplete with usernames                                   | ✅     |
| Autocomplete with @all & @here                                | ✅     |
| Autocomplete room/channel name                                | ✅     |
| Upload audio                                                  | ✅     |
| Forgot your password                                          | ✅     |
| Login screen: terms of service                                | ✅     |
| Login screen: privacy policy                                  | ✅     |
| Authentication via Google                                     | ✅     |
| Authentication via Facebook                                   | ✅     |
| Authentication via Twitter                                    | ✅     |
| Authentication via GitHub                                     | ✅     |
| Authentication via GitLab                                     | ✅     |
| Authentication via LinkedIn                                   | ✅     |
| Create channel                                                | ✅     |
| Search Local                                                  | ✅     |
| Search in the API                                             | ✅     |
| Settings -> License                                           | ✅     |
| Settings -> App version                                       | ✅     |
| Autocomplete emoji                                            | ✅     |
| Upload file (documents, PDFs, spreadsheets, zip files, etc)   | ✅     |
| Copy message                                                  | ✅     |
| Pin message                                                   | ✅     |
| Unpin message                                                 | ✅     |
| Channel Info screen -> Members                                | ✅     |
| Channel Info screen -> Pinned                                 | ✅     |
| Channel Info screen -> Starred                                | ✅     |
| Channel Info screen -> Uploads                                | ✅     |
| Star message                                                  | ✅     |
| Unstar message                                                | ✅     |
| Channel Info screen -> Topic                                  | ✅     |
| Channel Info screen -> Description                            | ✅     |
| Star a channel                                                | ✅     |
| Message format: videos                                        | ✅     |
| Message format: audios                                        | ✅     |
| Edit message                                                  | ✅     |
| Delete a message                                              | ✅     |
| Reply message                                                 | ✅     |
| Quote message                                                 | ✅     |
| Muted state                                                   | ✅     |
| Offline reading                                               | ✅     |
| Offline writing                                               | ✅     |
| Edit profile                                                  | ✅     |
| Reactions                                                     | ✅     |
| Custom emojis                                                 | ✅     |
| Accessibility (Basic)                                         | ✅     |
| Tap notification, go to the channel                           | ✅     |
| Deep links: Authentication                                    | ✅     |
| Deep links: Rooms                                             | ✅     |
| Full name setting                                             | ✅     |
| Read only rooms                                               | ✅     |
| Typing status                                                 | ✅     |
| Create channel/group                                          | ✅     |
| Disable registration setting                                  | ✅     |
| Unread red line indicator on message list                     | ✅     |
| Search Messages in Channel                                    | ✅     |
| Mentions List                                                 | ✅     |
| Attachment List                                               | ✅     |
