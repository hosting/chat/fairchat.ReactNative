import { isIOS } from '../utils/deviceInfo';

const APP_STORE_ID = '1301616129';
const BUNDLE_ID = 'com.osalliance.rocketchatMobile';

export const PLAY_MARKET_LINK = `https://play.google.com/store/apps/details?id=${BUNDLE_ID}`;
export const APP_STORE_LINK = `https://itunes.apple.com/app/id${APP_STORE_ID}`;
export const LICENSE_LINK = 'https://github.com/RocketChat/Rocket.Chat.ReactNative/blob/develop/LICENSE';
export const STORE_REVIEW_LINK = isIOS
  ? `itms-apps://itunes.apple.com/app/id${APP_STORE_ID}?action=write-review`
  : `market://details?id=${BUNDLE_ID}`;
export const FEEDBACK_LINK = 'https://git.fairkom.net/chat/fairchat.ReactNative/issues/';
export const DONATE_LINK = 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N8PYR9YWQHVE8&source=url';
export const FAIRAPPS_LINK = 'https://fairapps.net';
